# Back-end Socket App

This is the first time template of backend multi-threads app for stock broker and adviser company. 
It was designed as producer of ticker data to clients React Native apps via TLSv1.2 protocols.
No Spring Framework as a condition for beginning :(

### Technologies
- Maven Profiles
- Unirest
- SSLServerSocketFactory with TLSv1.2 cryptographic protocol
- JUnit 5 for tests
- java.util.Optional
- java.util.concurrent.CompletableFuture

### Start build with tests
mvn clean install

### Start deploy build
mvn -Pdeploy clean install

### Start deploy jar
java -Djavax.net.ssl.keyStore="path" -Djavax.net.ssl.keyStorePassword="pass" -jar ...




