package org.romana;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Optional;
import java.util.Properties;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import kong.unirest.Unirest;
import kong.unirest.UnirestInstance;

public class HttpRequestService {

	static String response;
	static String requestTickers="";
	static Properties appProps; 

	protected static String request(Properties appProps) {
			buildRequestString(appProps);
			unirestRequest(appProps);
			return response;
	}

	protected static void buildRequestString(Properties appProps) {
		final Object[] templateParams = { 
				Optional.ofNullable(appProps.getProperty("Ticker1")).orElse(""),
				Optional.ofNullable(appProps.getProperty("Ticker2")).orElse(""),
				Optional.ofNullable(appProps.getProperty("Ticker3")).orElse("") };
		final String template = "https://sandbox.tradier.com/v1/markets/quotes?symbols=%s%s%s";
		requestTickers = String.format(template, templateParams);
	}

	protected static void unirestRequest(Properties appProps) {
			UnirestInstance unirest = Unirest.primaryInstance();
			// It can be configured and used just like the static context
				unirest.config().connectTimeout(5000);

				unirest.get(requestTickers).header("accept", "application/json")
					.header("Authorization", appProps.getProperty("Bearer")).asJson()
					.ifSuccess(r -> {
							String listJSONTickers = r.getBody().getObject().getJSONObject("quotes").getJSONArray("quote").toString();
							Type listType = new TypeToken<List<Ticker>>() {
							}.getType();
							List<Ticker> listItemsDes = new Gson().fromJson(listJSONTickers, listType);
							response = new Gson().toJson(listItemsDes);
					}).ifFailure(r -> {
							response = r.getBody().getObject().toString();
							System.out.printf("error %s %n", response);
							response=null;
					});
				Unirest.shutDown();
	}
}