package org.romana;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Ticker implements Serializable {

    @SerializedName("symbol")
    @Expose
    private String symbol;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("last")
    @Expose
    private Double last;
    @SerializedName("change")
    @Expose
    private Double change;
    @SerializedName("open")
    @Expose
    private Double open;
    @SerializedName("change_percentage")
    @Expose
    private Double changePercentage;
    private final static long serialVersionUID = 5243965253862509454L;

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getLast() {
        return last;
    }

    public void setLast(Double last) {
        this.last = last;
    }

    public Double getChange() {
        return change;
    }

    public void setChange(Double change) {
        this.change = change;
    }

    public Double getOpen() {
        return open;
    }

    public void setOpen(Double open) {
        this.open = open;
    }

    public Double getChangePercentage() {
        return changePercentage;
    }

    public void setChangePercentage(Double changePercentage) {
        this.changePercentage = changePercentage;
    }

    @Override
    public String toString() {
        return new StringBuilder().append(" symbol=").append(symbol).append(" description=").append(description)
                .append(" last=").append(last).append(" change").append(change).append(" open=").append(open)
                .append(" changePercentage=").append(changePercentage).toString();
    }

}