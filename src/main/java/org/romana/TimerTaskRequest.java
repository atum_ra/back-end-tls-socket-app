package org.romana;

import java.util.Properties;
import java.util.TimerTask;
import static org.romana.TlsSocketServer.cache; 

public class TimerTaskRequest extends TimerTask {

    protected Properties appProps;

    protected TimerTaskRequest (Properties appProps) {
        this.appProps=appProps;
    }

    @Override
    public void run() {
        String response = HttpRequestService.request(appProps);

        if (response != null) cache.put("first", response);
        else cache.put("first", "No data downloaded due the fail connection");
        }
    }
    
