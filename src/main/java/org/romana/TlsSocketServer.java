package org.romana;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.net.ServerSocketFactory;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;

public class TlsSocketServer implements Runnable {

    private final static int serverPort = 3001;

    // only a single writer
    protected volatile static Map<String, String> cache = new ConcurrentHashMap<>();
    protected static Properties appProps = new Properties();

    public TlsSocketServer() throws IOException {
    }

    public static void main(String[] args) throws InterruptedException {
        getProperties();

        TimerTask repeatedTask = new TimerTaskRequest(appProps);
        Timer requestTickers = new Timer(true);
        requestTickers.scheduleAtFixedRate(repeatedTask, 0, TimeUnit.HOURS.toMillis(12));

        try {
            Thread t = new Thread(new TlsSocketServer());
            t.start();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Lifetime of app
        Thread.sleep(TimeUnit.DAYS.toMillis(60));
        System.exit(0);

    }

    public void run() {
        while (true) {
            try {

                ServerSocketFactory serverSocketFactory = SSLServerSocketFactory.getDefault();
                SSLServerSocket serverSocket = (SSLServerSocket) serverSocketFactory.createServerSocket(serverPort);
                serverSocket.setEnabledProtocols(new String[] { "TLSv1.2" });
                Socket s = serverSocket.accept();

                DataOutputStream out = new DataOutputStream(s.getOutputStream());
                // Create new thread pool for handling socket
                ExecutorService executor = Executors.newSingleThreadExecutor();

                // Future can help to catch OutOfMemory Error
                CompletableFuture<Void> future = CompletableFuture.runAsync(() -> {
                    // Code which might throw an exception
                    try {
                        String responseOutputStream;
                        responseOutputStream = cache.get("first");
                        out.writeUTF(responseOutputStream);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }, executor);

                try {
                    future.get(1000, TimeUnit.MILLISECONDS);
                    executor.shutdown();
                } catch (InterruptedException | ExecutionException | TimeoutException e) {
                    e.printStackTrace();
                } finally {
                    future.cancel(true);
                    executor.shutdownNow();
                }

                s.close();
                serverSocket.close();
                System.out.println("Socket closed");

            } catch (IOException e) {
                System.out.println("Server exception: " + e.getMessage());
                e.printStackTrace();
                break;
            }
        }
    }

    public static void getProperties() {
        InputStream property = null;
        try {
            property = TlsSocketServer.class.getClassLoader().getResourceAsStream("application.properties");
            appProps.load(property);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (System.getProperty("javax.net.ssl.nopass") == null) {
            try {
                String keyStore = Optional.ofNullable(appProps.getProperty("javax.net.ssl.keyStore"))
                        .orElseThrow(() -> new Exception());
                System.setProperty("javax.net.ssl.keyStore", keyStore);
                System.setProperty("javax.net.ssl.keyStorePassword", "nopass");

            } catch (Exception e) {
                System.out.printf("You need to add keyStore, keyStorePassword for TLSv1.2 protocol, exit.%n");
                System.exit(0);
            }
        }   
    }

}
