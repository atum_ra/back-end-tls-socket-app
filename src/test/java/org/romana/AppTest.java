package org.romana;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;



/**
 * Unit test for simple App.
 */
public class AppTest {
    static Properties appProps;
    
    
    @BeforeAll
    static void setup() {
        System.out.println("@BeforeAll executed");
        appProps = new Properties();
        InputStream property = null;
        try {
            property = TlsSocketServer.class.getClassLoader().getResourceAsStream("application.properties");
            appProps.load(property);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @BeforeEach
    void setupThis() {
        System.out.println("@BeforeEach executed");
    }

    @DisplayName("HttpRequestService")
    @Test
    public void WhenRequestFromVendorsOk() {
        String response="[{\"symbol\":\"ABC\",\"description\":\"AmerisourceBergen Corp\",\"last\":93.6,\"change\":1.66,\"open\":91.7,\"change_percentage\":1.81},"
        +"{\"symbol\":\"MCK\",\"description\":\"McKesson Corp\",\"last\":162.14,\"change\":5.82,\"open\":161.61,\"change_percentage\":3.63}]";
        HttpRequestService.requestTickers="http://www.mocky.io/v2/5e45c8323000002648614f2d";

        HttpRequestService.unirestRequest(appProps);

        assertEquals(response, HttpRequestService.response);
    }

}
